# Manifest of the not-existing art

  &copy; 2014 DPD- (Davide Peressoni) & Yuri Pellegrini

- Non-existent art can be realised using the most various techniques (drawing, sculpture, writing, ...).
- Non-existent art's objective is to represent the subject of the creation so that it is not physically present and everyone can imagine it as they like it.  
It is inevitable that the creations will eventually appear all the same, but they will have distinct sizes, the frame (or the base...) and especially the author and the title: the title, in fact, gives the public guidelines on what to imagine.
- Since the signature cannot be placed on the piece, due to physical motivations, it can be written on a description label put on the frame, base, ...
- Distinction between creations comes through the following parameters:
  * Title
  * Technique
  * Sizes
- Since it is apparently not so difficult to create such creations, artists are invited not to abuse of non-existent art, but to deeply reason on their creations.

# Manifesto dell'arte non presenzista

  &copy; 2014 DPD- (Davide Peressoni) & Yuri Pellegrini

- L'arte non presenzista si può realizzare attraverso le più disparate tecniche (disegno, scultura, scrittura, ...).
- L'arte non presenzista ha come obiettivo rappresentare il soggetto dell'oggetto artistico in modo che non sia presente fisicamente e ognuno può immaginarselo come meglio gli pare.  
È inevitabile che le opere risulteranno tutti apparentemente uguali, ma avranno di distintivo le dimensioni, la cornice (o la base,...)  e soprattutto l'artista e il titolo: il titolo infatti è quello che dice a colui che guarda l'opera cosa rappresenta e cosa deve immaginarsi.
- La firma sull'opera non può essere apposta per motivi fisici, quindi si può apporre la firma nella targhetta descrittiva posta sulla cornice, sulla base,...
- La distinzione di un'opera dall'altra avviene attraverso i seguenti parametri:
  * Titolo
  * Tecnica
  * Dimensione
- Dato che è apparentemente facile creare opere di questa corrente, si invita gli artisti a non abusare del non presenzismo, ma bensì; a ragionare approfonditamente sulla creazione delle opere.
